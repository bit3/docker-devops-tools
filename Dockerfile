FROM alpine

RUN set -xeo pipefail \
    && apk upgrade --update \
    && apk add \
            bash \
            bind-tools \
            bzip2 \
            curl \
            git \
            grep \
            gzip \
            jq \
            openssh \
            rsync \
            sqlite \
            tar \
            tcpdump \
            vim \
            wget \
            xmlstarlet \
            xz \
            zsh \
    && sed -i 's~root:x:0:0:root:/root:/bin/ash~root:x:0:0:root:/root:/bin/zsh~' /etc/passwd \
    && SHELL=/bin/zsh ZSH=/opt/oh-my-zsh sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" \
    && rm -rf /var/cache/apk/* /opt/oh-my-zsh/.git 

COPY zshrc /etc/zsh/zshrc

CMD ["zsh"]
