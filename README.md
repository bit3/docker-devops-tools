devops tools
============

[![repository](https://img.shields.io/badge/repository-bit3%2Fdocker--devops--tools-brightgreen.svg)](https://gitlab.com/bit3/docker-devops-tools) [![pipeline status](https://gitlab.com/bit3/docker-devops-tools/badges/master/pipeline.svg)](https://gitlab.com/bit3/docker-devops-tools/commits/master) [![image layers](https://images.microbadger.com/badges/image/bit3/devops-tools.svg)](https://microbadger.com/images/bit3/devops-tools "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/bit3/devops-tools.svg)](https://hub.docker.com/r/bit3/devops-tools/) [![Docker Pulls](https://img.shields.io/docker/pulls/bit3/devops-tools.svg)](https://hub.docker.com/r/bit3/devops-tools/)


Ultra lightweight docker image for devops - based on alpine linux - running in a fancy [`oh-my-zsh`](http://ohmyz.sh/) shell.

Packages
========

- bash
- bind-tools
- bzip2
- curl
- git
- grep
- gzip
- jq
- openssh 
- rsync
- sqlite
- tar
- tcpdump
- vim
- wget
- xmlstarlet
- xz
- zsh

You need another package? Let me know, send an [e-mail](mailto:incoming+bit3/devops-tools@incoming.gitlab.com) or create a ticket on [gitlab.com/bit3/docker-devops-tools](https://gitlab.com/bit3/docker-devops-tools/issues/new).

Usage examples
==============

Run a shell
-----------

Launch a shell with the volumes of another container to view, organize, and edit the files.

```
docker run --rm -it \
  --volumes-from another-container \
  bit3/devops-tools
```

rsync files
-----------

Sync the files between two containers.

```
docker run --rm -it \
  --volume volume_a:/volume_a \
  --volume volume_b:/volume_b \
  bit3/devops-tools rsync -av --delete /volume_a/ /volume_b/  
```

Create backup of volume
-----------------------

For example, to back up the database files.

```
docker run --rm -it \
  --volume /var/backup:/backup \
  --volumes-from any-service \
  bit3/devops-tools tar -cJf /backup/any-service-backup.tar.xz -C /path/to/files/to/backup .
```

Restore volume from backup
--------------------------

```
docker run --rm -it \
  --volume /var/backup:/backup \
  --volumes-from any-service \
  bit3/devops-tools tar -xJf /backup/any-service-backup.tar.xz -C /path/to/files/to/restore
```
